package usthon.syafii.appx0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterSembako(val dataSem : List<HashMap<String, String>>,
                     val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterSembako.HolderSembako>(){
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterSembako.HolderSembako {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_buku,p0,false)
        return HolderSembako(v)
    }

    override fun getItemCount(): Int {
        return dataSem.size
    }

    override fun onBindViewHolder(p0: AdapterSembako.HolderSembako, p1: Int) {
        val data = dataSem.get(p1)
        p0.txKode.setText(data.get("kode"))
        p0.txJudul.setText(data.get("judul"))
        p0.txPenerbit.setText(data.get("penerbit"))
        p0.txTahun.setText(data.get("tahun"))
        p0.txKategori.setText(data.get("kategori"))
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener({
            val pos = mainActivity.daftarSatuan.indexOf(data.get("kategori"))
            mainActivity.spinner.setSelection(pos)
            mainActivity.edKode.setText(data.get("kode"))
            mainActivity.edJudul.setText(data.get("judul"))
            mainActivity.edPenerbit.setText(data.get("penerbit"))
            mainActivity.edTahun.setText(data.get("tahun"))
            Picasso.get().load(data.get("url")).into(mainActivity.imgUpload)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)

    }
    class HolderSembako(v : View) : RecyclerView.ViewHolder(v){
        val txKode = v.findViewById<TextView>(R.id.txKode)
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val txPenerbit = v.findViewById<TextView>(R.id.txPenerbit)
        val txTahun = v.findViewById<TextView>(R.id.txTahun)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

}